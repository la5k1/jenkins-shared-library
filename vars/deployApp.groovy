#!/usr/bin/env groovy

def call() {
    def ec2Instance = "admin@158.160.116.222"
    sshagent(['ssh-myvm-key']) {
        sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ${ec2Instance}:/home/admin"
        sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} 'export IMAGE=$IMAGE_NAME; docker-compose -f docker-compose.yaml up --detach'"
    }
}