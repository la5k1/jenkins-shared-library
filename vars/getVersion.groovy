#!/usr/bin/env groovy

def call() {
    def prevVersion = sh (returnStdout: true, script: "cat package.json | grep version | cut -d '\"' -f 4 | tr -d '\\n'")
    def version = "$prevVersion-$BUILD_NUMBER"
    return version
}
