#!/usr/bin/env groovy
package com.example

class Node implements Serializable {

    def script

    Node(script) {
        this.script = script
    }

    def nodeIncrement() {
        script.echo "incrementing version..."
        script.sh "npm version minor"
    }


    def nodeRunTests() {
        script.sh "npm install"
        script.sh "npm run test"
    }

}